; @gogl 
GOGL(id=-1)
{
	ControlSend ,,{vk38}, ahk_id %id%  ; 8
}

; @go some city
GOLOC(id=-1)
{

	ControlSend ,,{vk39}, ahk_id %id%  ; 9
}


; turn off spell (switch weapons)
BARDSPELLOFF(id)
{
	ControlSend ,,{vk53}, ahk_id %id%
	Sleep, 500
	ControlSend ,,{vk44}, ahk_id %id%
	Sleep, 100
}

; turn off spell (switch weapons)
DANCERSPELLOFF(id)
{
	ControlSend ,,{F7}, ahk_id %id%
	Sleep, 500
	ControlSend ,,{F8}, ahk_id %id%
	Sleep, 100
}

BARDSPELL1(id)
{
	BARDSPELLOFF(id)
	ControlSend ,,{F5}, ahk_id %id%
}	


DANCERSPELL1(id) 
{
	DANCERSPELLOFF(id)
	ControlSend ,,{F4}, ahk_id %id%
}	

BARDSPELL2(id)
{
	BARDSPELLOFF(id)
	ControlSend ,,{vk4b}, ahk_id %id%
}	


DANCERSPELL2(id)
{
	DANCERSPELLOFF(id)
	ControlSend ,,{F6}, ahk_id %id%
}	


BARDENCORE(id)
{
	ControlSend ,,{F9}, ahk_id %id%
}

DANCERENCORE(id)
{
	ControlSend ,,{F9}, ahk_id %id%
}

GOSPEL(id)
{                       
	ControlSend ,,{F9}, ahk_id %id%
}
