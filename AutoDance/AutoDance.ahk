#ifwinActive RagnaChan game client | Gepard Shield 2.0 (^-_-^)

#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
; SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

#include ButtonSettings.ahk

Gui,+AlwaysOnTop
Gui, Add, Button, x0 y0 w60 h20 ,BARD
Gui, Add, Button, x0 y20 w60 h20 ,DANCER
Gui, Add, Button, x0 y40 w60 h20 ,GOSPEL
Gui, Add, Button, x0 y60 w60 h20 ,OTHER1
Gui, Add, Button, x0 y80 w60 h20 ,OTHER2

Gui, Add, Button, x180 y80 w60 h20 ,RELOAD
Gui, Add, Button, x240 y80 w60 h20 ,PAUSE
Gui, Add, Button, x300 y80 w60 h20 ,RESUME

Gui, Add, Button, x60 y0 w60 h20, RESB
Gui, Add, Button, x60 y20 w60 h20, RESD
Gui, Add, Button, x60 y40 w60 h20, RESG

Gui, Add, Button, x120 y0 w60 h20, SPELLB1
Gui, Add, Button, x120 y20 w60 h20, SPELLD1
Gui, Add, Button, x120 y40 w60 h20, SPELLG

Gui, Add, Button, x180 y0 w60 h20, SPELLB2
Gui, Add, Button, x180 y20 w60 h20, SPELLD2

Gui, Add, Button, x240 y0 w60 h20, LOCB
Gui, Add, Button, x240 y20 w60 h20, LOCD
Gui, Add, Button, x240 y40 w60 h20, LOCG

Gui, Add, Button, x300 y00 w60 h40 ,SPELLOFF


Gui, Show, ,Auto Dance With Me

return

ButtonPAUSE:
	Pause, On
return

ButtonRESUME:
	Pause, Off	
return


w1id:=-1
w2id:=-1
w3id:=-1
w4id:=-1
w5id:=-1

w1pid:=-1
w2pid:=-1
w3pid:=-1

^+F1::
	WinGet w1id, ID
	WinGet w1pid, PID
	MsgBox, Bard Saved

	SetTimer BardEncore, 300
	SetTimer BardRes, 10000
return

^+F2::
	WinGet w2id, ID
	WinGet w2pid, PID
	MsgBox, Dancer Saved

	SetTimer DancerEncore, 300
	SetTimer DancerRes, 10000
return

^+F3::
	WinGet w3id, ID
	WinGet w3pid, PID
	MsgBox, Gospel Saved

	SetTimer Gospel, 60200
	SetTimer GospelRes, 10000
return

^+F4::
	WinGet w4id, ID
	MsgBox, Other1 Saved

return

^+F5::
	WinGet w5id, ID
	MsgBox, Other2 Saved

return

BardEncore:
	BARDENCORE(w1id)
return

DancerEncore:
	DANCERENCORE(w2id)
return

Gospel:
	GOSPEL(w3id)
return

bard_res:=0
dancer_res:=0
gospel_res:=0

BardRes:
	hp:=readMemory(0x83e1b4, w1pid)
	if(hp=0)
	{
		if(bard_res = 0)
		{
			GOGL(w1id)
		}
		else
		{
			GOLOC(w1id)
		}
	}
return


DancerRes:
		hp:=readMemory(0x83e1b4, w2pid)
	if(hp=0)
	{
		if(dancer_res = 0)
		{
			GOGL(w2id)
		}
		else
		{
			GOLOC(w2id)
		}
	}
return

GospelRes:
	hp:=readMemory(0x83e1b4, w3pid)
	if(hp=0)
	{
		if(gospel_res = 0)
		{
			GOGL(w3id)
		}
		else
		{
			GOLOC(w3id)
		}
	}
return


ButtonBARD:
	WinActivate, ahk_id %w1id%
	Sleep, 200
	WinGet, id, ID
return

ButtonDANCER:
	WinActivate, ahk_id %w2id%
	Sleep, 200
	WinGet, id, ID
return

ButtonGOSPEL:
	WinActivate, ahk_id %w3id%
		Sleep, 200
	WinGet, id, ID
return

ButtonOTHER1:
	WinActivate, ahk_id %w4id%
		Sleep, 200
	WinGet, id, ID
return

ButtonOTHER2:
	WinActivate, ahk_id %w5id%
		Sleep, 200
	WinGet, id, ID
return



ButtonRESB:
	GOGL(w1id)
	bard_res := 0
return

ButtonRESD:
	GOGL(w2id)
	dancer_res := 0
return

ButtonRESG:
	GOGL(w3id)
	gospel_res := 0
return

ButtonLOCB:
	GOLOC(w1id)
	bard_res := 1
return

ButtonLOCD:
	GOLOC(w2id)
	dancer_res := 1
return

ButtonLOCG:
	GOLOC(w3id)
	gospel_res := 1
return

ButtonSPELLB1:
	SetTimer BardEncore, Delete
	BARDSPELL1(w1id)
	SetTimer BardEncore, 300
return

ButtonSPELLD1:
	SetTimer DancerEncore, Delete
	DANCERSPELL1(w2id)
	SetTimer DancerEncore, 300
return

ButtonSPELLG:
	SetTimer Gospel, Delete
	GOSPEL(w3id)
	SetTimer Gospel, 60200	
return

ButtonSPELLB2:
	SetTimer BardEncore, Delete
	BARDSPELL2(w1id)
	SetTimer BardEncore, 300
return

ButtonSPELLD2:
	SetTimer DancerEncore, Delete
	DANCERSPELL2(w2id)
	SetTimer DancerEncore, 300
return


ButtonSPELLOFF:
	SetTimer Gospel, Delete
	SetTimer BardEncore, Delete
	SetTimer DancerEncore, Delete

	BARDSPELLOFF(w1id)
	DANCERSPELLOFF(w2id)
return

ButtonRELOAD:
	Reload
	return

GuiClose:
GuiEscape:
	ExitApp





ReadMemory(MADDRESS=0,pid=0,BYTES=4)
{
   Static OLDPROC, ProcessHandle
   Static OLDPID
   VarSetCapacity(MVALUE, BYTES,0)
   If pid != %OLDPID%
   {
      ProcessHandle := ( ProcessHandle ? 0*(closed:=DllCall("CloseHandle"
      ,"UInt",ProcessHandle)) : 0 )+(pid ? DllCall("OpenProcess"
      ,"Int",16,"Int",0,"UInt",pid) : 0)
   }
   If (ProcessHandle) && DllCall("ReadProcessMemory","UInt",ProcessHandle,"UInt",MADDRESS,"Str",MVALUE,"UInt",BYTES,"UInt *",0)
   {    Loop % BYTES
            Result += *(&MVALUE + A_Index-1) << 8*(A_Index-1)
        Return Result
    }
   return !ProcessHandle ? "Handle Closed:" closed : "Fail"
}
