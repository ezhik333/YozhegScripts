#ifwinActive RagnaChan game client | Gepard Shield 2.0 (^-_-^)

#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
; SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

CoordMode, Mouse, Client
CoordMode, Pixel, Client

#include ButtonSettings.ahk


; настройки кнопок:
; F2 - абра, F3 - cast cancel, Alt-1 - @gogl
; для поиска с целью изменения (пометки в комментариях):
; $_ABRA $_CC $_GOGL
;
; Num / -- Старт
; Num * -- Стоп (длинное нажатие)
; Num - -- Переместиться на респ (от залипаний и глюков)
; $_START $_STOP $_REFRESH
;
; стоит попробовать прописать кнопки как переменные. пока не нашел как

TrayTip, Yozheg's Abra, Press CTRL-F1 in abra window to start, 5


; не сделано: бэкстеб

; разные окна
abra_pid:=-1

IniRead, anchor1_x, abra.ini, Abra, Anchor1X
IniRead, anchor1_y, abra.ini, Abra, Anchor1Y
IniRead, anchor1_c, abra.ini, Abra, Anchor1C
IniRead, anchor2_x, abra.ini, Abra, Anchor2X
IniRead, anchor2_y, abra.ini, Abra, Anchor2Y
IniRead, anchor2_c, abra.ini, Abra, Anchor2C
IniRead, abyss1_x, abra.ini, Abra, Abyss1X
IniRead, abyss1_y, abra.ini, Abra, Abyss1Y
IniRead, abyss1_c, abra.ini, Abra, Abyss1C
IniRead, abyss2_x, abra.ini, Abra, Abyss2X
IniRead, abyss2_y, abra.ini, Abra, Abyss2Y
IniRead, abyss2_c, abra.ini, Abra, Abyss2C
IniRead, healer_x, abra.ini, Abra, HealerX
IniRead, healer_y, abra.ini, Abra, HealerY
IniRead, longspell_x, abra.ini, Abra, LongspellX
IniRead, longspell_y, abra.ini, Abra, LongspellY
IniRead, longspell_c, abra.ini, Abra, LongspellC
IniRead, search1_x, abra.ini, Abra, Search1X
IniRead, search1_y, abra.ini, Abra, Search1Y
IniRead, search2_x, abra.ini, Abra, Search2X
IniRead, search2_y, abra.ini, Abra, Search2Y
IniRead, pharmacy1_x, abra.ini, Abra, Pharmacy1X
IniRead, pharmacy1_y, abra.ini, Abra, Pharmacy1Y
IniRead, pharmacy1_c, abra.ini, Abra, Pharmacy1C
IniRead, pharmacy2_x, abra.ini, Abra, Pharmacy2X
IniRead, pharmacy2_y, abra.ini, Abra, Pharmacy2Y
IniRead, mkarrow1_x, abra.ini, Abra, Mkarrow1X
IniRead, mkarrow1_y, abra.ini, Abra, Mkarrow1Y
IniRead, mkarrow1_c, abra.ini, Abra, Mkarrow1C
IniRead, mkarrow2_x, abra.ini, Abra, Mkarrow2X
IniRead, mkarrow2_y, abra.ini, Abra, Mkarrow2Y
IniRead, vending1_x, abra.ini, Abra, Vending1X
IniRead, vending1_y, abra.ini, Abra, Vending1Y
IniRead, vending1_c, abra.ini, Abra, Vending1C
IniRead, vending2_x, abra.ini, Abra, Vending2X
IniRead, vending2_y, abra.ini, Abra, Vending2Y


start_check:=1
gems_check:=1

Started:=0

; НАЧАЛО РАБОТЫ
Loop
{

	if (Started = 0)
		continue

	if (abra_pid = -1)
	{
		MsgBox, Choose Window and press Ctrl+F1
		Started:=0
		continue

	}

	WinGetActiveStats, Title, Width, Height, X, Y

	Sleep, 500

	Loop
	{

		IfWinNotActive, ahk_id %abra_pid%
		{
			Sleep, 100
			continue
		}

		; Длинное нажтие  Num * - выключение
		;if GetKeyState("vk6a", "P") ; Num *    $_STOP
		;{
	;		MsgBox, STOPPED
	;		break
	;	}


		; поиск и закрытие всплывающего окна pharmacy
		PixelGetColor, Color1w, pharmacy1_x, pharmacy1_y
		if(Color1w = pharmacy1_c)
		{
			IfWinNotActive, ahk_id %abra_pid%
			{
				Sleep, 100
				continue
			}

			MouseMove, pharmacy2_x, pharmacy2_y
			Sleep, 100
			MouseClick, left
			Sleep, 1000
			
			ToolTip, Pharmacy closed, 250, 50

			continue
		}

		; поиск и закрытие всплывающего окна
		PixelGetColor, Color1w, mkarrow1_x, mkarrow1_y
		if(Color1w = mkarrow1_c)
		{
			IfWinNotActive, ahk_id %abra_pid%
			{
				Sleep, 100
				continue
			}

			MouseMove, mkarrow2_x, mkarrow2_y
			Sleep, 100
			MouseClick, left
			Sleep, 1000

			ToolTip, Make Arrow closed, 250, 50
			
			continue
		}


		; поиск хайда
		ImageSearch, FoundX, FoundY, Width-60, 180, Width-10, Height, hide.bmp

		; ошибка - ругаемся
		if ErrorLevel = 2
		{
			MsgBox, ERROR
		}

		; нашли - обновляемся
		if ErrorLevel = 0
		{
			ToolTip, Hiding avoid, 250, 50

			GOGL(abra_pid)
			Sleep, 1000
			continue
		}

		; поиск трикдеда
		ImageSearch, FoundX, FoundY, Width-60, 180, Width-10, Height, trick.bmp

		; ошибка - ругаемся
		if ErrorLevel = 2
		{
			MsgBox, ERROR2
		}

		; нашли - обновляемся
		if ErrorLevel = 0
		{

			ToolTip, Trick Dead avoid, 250, 50

			GOGL(abra_pid)
			Sleep, 1000
			continue
		}

		; проверка что мы на респе
		if (start_check = 1)
		{
			PixelGetColor, Color1a, anchor1_x, anchor1_y

			NeedMove:=0
			if not (Color1a = anchor1_c)
			{
				NeedMove:=NeedMove + 1
			}

			PixelGetColor, Color2a, anchor2_x, anchor2_y

			if not (Color2a = anchor2_c) 
			{
				NeedMove:=NeedMove + 1
			}


			; если на респе - перемещаемся в угол
			if not (NeedMove = 0)
			{		
				ToolTip, Anchor failed - go resp, 250, 50

				IfWinNotActive, ahk_id %abra_pid%
				{
					Sleep, 100
					continue
				}

				GOGL(abra_pid)
				Sleep, 1000
			}

			Sleep, 10


		}


		; проверка на абис
		if(gems_check = 1)
		{
			PixelGetColor, Color1b, abyss1_x, abyss1_y

			FailGems:=1

			if (Color1b = abyss1_c)
			{
				FailGems:=0
			}

			PixelGetColor, Color2b, abyss2_x, abyss2_y

			if (Color2b = abyss2_c)
			{
				FailGems:=0
			}


			; если абиса нет - ждем...
			if FailGems=1
			{
				ToolTip, No Abyss, 250, 50

				;MsgBox, %X% %Y%
				;MsgBox, %Color1a% %Color2a%
				;MsgBox, %Color1b% %Color2b%
				;MsgBox, GEMS
				
				SoundPlay Sound2.wav
				Sleep, 2000

				continue
			}
		}





		; проверка ХП и СП
		needheal:=0

		hp:=readMemory(0x83e1b4)
		sp:=readMemory(0x83e1bc)

		if(hp = 0)
		{
			ToolTip, Dead, 250, 50

			GOGL(abra_pid)
			Sleep, 1000

			continue	
		}

		if(hp < 100)
			needheal:= 1

		if(sp < 100)
			needheal:= 1

		if(needheal = 1)
		{

			IfWinNotActive, ahk_id %abra_pid%
			{
				Sleep, 100
				continue
			}

			; каст кансел
			CASTCANCEL(abra_pid)
			Sleep, 200

			ToolTip, Need Rebuff and Heal, 250, 50

			; кликаем на хилера
			MouseMove, healer_x, healer_y
			Sleep, 100
			Send, {Alt down}
			Sleep, 100
			MouseClick, left
			Sleep, 100
			Send, {Alt up}

			; continue
		}


		; проверка открытого вендинга
		PixelGetColor, Color1v, vending1_x, vending1_y
		if (Color1v = vending1_c)
		{
			ToolTip, Vending closed, 250, 50

			MouseMove, vending2_x, vending2_y
			Sleep, 100
			MouseClick, left
			Sleep, 1000

			continue
		}



		; АБРИМ ТУТ
		; АБРИМ ТУТ
		; АБРИМ ТУТ

		if (needheal = 0)
		{
			ABRA(abra_pid)		
		}


		; проверка на длинные касты
		PixelGetColor, ColorX, %longspell_x%, %longspell_y%


		; задержка между абрами, можно уменьшать и увеличивать но осторожно
		Sleep, 300
			
		; поиск вопросиков
		ImageSearch, FoundX, FoundY, search1_x, search1_y, search2_x, search2_y, Q.bmp

		; ошибка - ругаемся
		if ErrorLevel = 2
		{
			MsgBox, ERROR
		}

		; нашли вопросики - останавливаемся
		if ErrorLevel = 0
		{
			ToolTip, Abra success, 250, 50

			SoundPlay Sound.wav

			Started:=0
			Pause, On

			continue
		}

		if (ColorX = longspell_c)
		{
			CASTCANCEL(abra_pid)
			Sleep, 200

			continue
		}


		if(sp = sp_prev)
		{
			sp_fail:=sp_fail+1
		}
		else	
		{
			sp_fail:=0
		}

		sp_prev:=sp

		if(sp_fail = 10)
		{
			ToolTip, SP Changes failed - go resp, 250, 50

			sp_fail:=0
			GOGL(abra_pid)
			Sleep, 1000
		}
	}
}
return

$vk6f::	
	Pause, Off
	Started:=1
return

vk6a::
	Started:=0
	Pause, On
return

ButtonSTART:
	Pause, Off
	Started:=1	
	WinActivate, ahk_id %abra_pid%
return

ButtonSTOP:	
	Pause, On, 1
	Started:=0
return


; вернуть на старое место в случае залипания
vk6d:: ; Num -   $_REFRESH
	Send, {Alt down}
	Sleep, 100
	Send, {vk31} ; $_GOGL
	Sleep, 100
	Send, {Alt up}
	Sleep, 1000
return


ReadMemory(MADDRESS=0)
{
	Static OLDPROC, ProcessHandle
	VarSetCapacity(MVALUE,4,0)
	WinGet, pid, pid
	ProcessHandle := ( ProcessHandle ? 0*(closed:=DllCall("CloseHandle"
	,"UInt",ProcessHandle)) : 0 )+(pid ? DllCall("OpenProcess"
	,"Int",16,"Int",0,"UInt",pid) : 0)
	If (ProcessHandle) && DllCall("ReadProcessMemory","UInt"
	,ProcessHandle,"UInt",MADDRESS,"Str",MVALUE,"UInt",4,"UInt *",0)
	return *(&MVALUE+3)<<24 | *(&MVALUE+2)<<16 | *(&MVALUE+1)<<8 | *(&MVALUE)
	return !ProcessHandle ? "Handle Closed: " closed : "Fail"
}

^F11::
	
return



^F1::
	WinGet abra_pid, ID
	MsgBox, Abra Window Saved: ID %abra_pid%

	window := WinActive()

	Gui,+AlwaysOnTop
	Gui, Add, Button, x0 y0 w70 h20 ,START
	Gui, Add, Button, x0 y20 w70 h20 ,ANCHOR1
	Gui, Add, Button, x0 y40 w70 h20 ,ANCHOR2
	Gui, Add, Button, x0 y60 w70 h20 ,GEMS1
	Gui, Add, Button, x0 y80 w70 h20 ,GEMS2
	Gui, Add, Button, x0 y100 w70 h20 ,SEARCH1
	Gui, Add, Button, x0 y120 w70 h20 ,SEARCH2


	Gui, Add, Button, x70 y20 w70 h20 ,PHARMACY
	Gui, Add, Button, x70 y40 w70 h20 ,PHCLOSE
	Gui, Add, Button, x70 y60 w70 h20 ,MKARROW
	Gui, Add, Button, x70 y80 w70 h20 ,MKCLOSE
	Gui, Add, Button, x70 y100 w70 h20 ,VENDING
	Gui, Add, Button, x70 y120 w70 h20 ,VDCLOSE

	Gui, Add, Button, x140 y0 w70 h20 ,STOP
	Gui, Add, Button, x140 y20 w70 h20 ,HEALER
	Gui, Add, Button, x140 y40 w70 h20 ,LONG 
	Gui, Add, Button, x140 y60 w70 h20 ,SOUND1
	Gui, Add, Button, x140 y80 w70 h20 ,SOUND2
	Gui, Add, Button, x140 y100 w70 h20 ,SAVEINI
	Gui, Add, Button, x140 y120 w70 h20 ,RELOAD	
	
	WinGetActiveStats, Title, Width, Height, X, Y

	Gui, Show, , Yozheg's Auto Abra

return

clicked:=-1

^LButton::

	IfWinNotActive, ahk_id %abra_pid%
	{
		Send, {LButton}
		return
	}

	if(clicked <= 0)
	{
		Send, {LButton}
		return
	}

	if(clicked = 1)
	{
		MouseGetPos, xpos, ypos 
		PixelGetColor, Color, xpos, ypos
		Msgbox, ANCHOR 1 saved at X %xpos% Y %ypos%, Color - %Color%

		anchor1_x:=xpos
		anchor1_y:=ypos
		anchor1_c:=Color
	}

	if(clicked = 2)
	{
		MouseGetPos, xpos, ypos 
		PixelGetColor, Color, xpos, ypos
		Msgbox, ANCHOR 2 saved at X %xpos% Y %ypos%, Color - %Color%

		anchor2_x:=xpos
		anchor2_y:=ypos
		anchor2_c:=Color
	}

	if(clicked = 3)
	{
		MouseGetPos, xpos, ypos 
		PixelGetColor, Color, xpos, ypos
		Msgbox, ABYSS GEMS 1 saved at X %xpos% Y %ypos%, Color - %Color%

		abyss1_x:=xpos
		abyss1_y:=ypos
		abyss1_c:=Color
	}

	if(clicked = 4)
	{
		MouseGetPos, xpos, ypos 
		PixelGetColor, Color, xpos, ypos
		Msgbox, ABYSS GEMS 2 saved at X %xpos% Y %ypos%, Color - %Color%

		abyss2_x:=xpos
		abyss2_y:=ypos
		abyss2_c:=Color
	}

	if(clicked = 5)
	{
		MouseGetPos, xpos, ypos 
		PixelGetColor, Color, xpos, ypos
		Msgbox, HEALER saved at X %xpos% Y %ypos%

		healer_x:=xpos
		healer_y:=ypos
	}

	if(clicked = 6)
	{
		MouseGetPos, xpos, ypos 
		PixelGetColor, Color, xpos, ypos
		Msgbox, LONGSPELL saved at X %xpos% Y %ypos%, Color - %Color%

		longspell_x:=xpos
		longspell_y:=ypos
		longspell_c:=Color
	}

	if(clicked = 7)
	{
		MouseGetPos, xpos, ypos 
		Msgbox, SEARCH1 saved at X %xpos% Y %ypos%

		search1_x:=xpos
		search1_y:=ypos
	}

	if(clicked = 8)
	{
		MouseGetPos, xpos, ypos 
		Msgbox, SEARCH2 saved at X %xpos% Y %ypos%

		search2_x:=xpos
		search2_y:=ypos
	}

	if(clicked = 9)
	{
		MouseGetPos, xpos, ypos 
		PixelGetColor, Color, xpos, ypos		
		Msgbox, PHARMACY1 saved at X %xpos% Y %ypos%, Color - %Color%

		pharmacy1_x:=xpos
		pharmacy1_y:=ypos
		pharmacy1_c:=Color
	}

	if(clicked = 10)
	{
		MouseGetPos, xpos, ypos 
		Msgbox, PHARMACY2 saved at X %xpos% Y %ypos%

		pharmacy2_x:=xpos
		pharmacy2_y:=ypos
	}

	if(clicked = 11)
	{
		MouseGetPos, xpos, ypos 
		PixelGetColor, Color, xpos, ypos		
		Msgbox, MKARROW1 saved at X %xpos% Y %ypos%, Color - %Color%

		mkarrow1_x:=xpos
		mkarrow1_y:=ypos
		mkarrow1_c:=Color
	}

	if(clicked = 12)
	{
		MouseGetPos, xpos, ypos 
		Msgbox, MKARROW2 saved at X %xpos% Y %ypos%

		mkarrow2_x:=xpos
		mkarrow2_y:=ypos
	}

	if(clicked = 13)
	{
		MouseGetPos, xpos, ypos 
		PixelGetColor, Color, xpos, ypos		
		Msgbox, VENDING1 saved at X %xpos% Y %ypos%, Color - %Color%

		vending1_x:=xpos
		vending1_y:=ypos
		vending1_c:=Color
	}

	if(clicked = 14)
	{
		MouseGetPos, xpos, ypos 
		Msgbox, VENDING2 saved at X %xpos% Y %ypos%

		vending2_x:=xpos
		vending2_y:=ypos
	}

	clicked:=0
return


ButtonANCHOR1:
	clicked:=1
	WinActivate, ahk_id %abra_pid%
return

ButtonANCHOR2:
	clicked:=2
	WinActivate, ahk_id %abra_pid%
return

ButtonGEMS1:
	clicked:=3
	WinActivate, ahk_id %abra_pid%
return

ButtonGEMS2:
	clicked:=4
	WinActivate, ahk_id %abra_pid%
return

ButtonHEALER:
	clicked:=5
	WinActivate, ahk_id %abra_pid%
return

ButtonLONGSPELL:
	clicked:=6
	WinActivate, ahk_id %abra_pid%
return

ButtonSEARCH1:
	clicked:=7
	WinActivate, ahk_id %abra_pid%
return

ButtonSEARCH2:
	clicked:=8
	WinActivate, ahk_id %abra_pid%
return

ButtonPHARMACY:
	clicked:=9
	WinActivate, ahk_id %abra_pid%
return

ButtonPHCLOSE:
	clicked:=10
	WinActivate, ahk_id %abra_pid%
return

ButtonMKARROW:
	clicked:=11
	WinActivate, ahk_id %abra_pid%
return

ButtonMKCLOSE:
	clicked:=12
	WinActivate, ahk_id %abra_pid%
return

ButtonVENDING:
	clicked:=13
	WinActivate, ahk_id %abra_pid%
return

ButtonVDCLOSE:
	clicked:=14
	WinActivate, ahk_id %abra_pid%
return

ButtonRELOAD:
	Reload
	ExitApp
return

ButtonSAVEINI:
	IniWrite, %anchor1_x%, abra.ini, Abra, Anchor1X
	IniWrite, %anchor1_y%, abra.ini, Abra, Anchor1Y
	IniWrite, %anchor1_c%, abra.ini, Abra, Anchor1C
	IniWrite, %anchor2_x%, abra.ini, Abra, Anchor2X
	IniWrite, %anchor2_y%, abra.ini, Abra, Anchor2Y
	IniWrite, %anchor2_c%, abra.ini, Abra, Anchor2C
	IniWrite, %abyss1_x%, abra.ini, Abra, Abyss1X
	IniWrite, %abyss1_y%, abra.ini, Abra, Abyss1Y
	IniWrite, %abyss1_c%, abra.ini, Abra, Abyss1C
	IniWrite, %abyss2_x%, abra.ini, Abra, Abyss2X
	IniWrite, %abyss2_y%, abra.ini, Abra, Abyss2Y
	IniWrite, %abyss2_c%, abra.ini, Abra, Abyss2C
	IniWrite, %healer_x%, abra.ini, Abra, HealerX
	IniWrite, %healer_y%, abra.ini, Abra, HealerY
	IniWrite, %longspell_x%, abra.ini, Abra, LongspellX
	IniWrite, %longspell_y%, abra.ini, Abra, LongspellY
	IniWrite, %longspell_c%, abra.ini, Abra, LongspellC
	IniWrite, %search1_x%, abra.ini, Abra, Search1X
	IniWrite, %search1_y%, abra.ini, Abra, Search1Y
	IniWrite, %search2_x%, abra.ini, Abra, Search2X
	IniWrite, %search2_y%, abra.ini, Abra, Search2Y
	IniWrite, %pharmacy1_x%, abra.ini, Abra, Pharmacy1X
	IniWrite, %pharmacy1_y%, abra.ini, Abra, Pharmacy1Y
	IniWrite, %pharmacy1_c%, abra.ini, Abra, Pharmacy1C
	IniWrite, %pharmacy2_x%, abra.ini, Abra, Pharmacy2X
	IniWrite, %pharmacy2_y%, abra.ini, Abra, Pharmacy2Y
	IniWrite, %mkarrow1_x%, abra.ini, Abra, Mkarrow1X
	IniWrite, %mkarrow1_y%, abra.ini, Abra, Mkarrow1Y
	IniWrite, %mkarrow1_c%, abra.ini, Abra, Mkarrow1C
	IniWrite, %mkarrow2_x%, abra.ini, Abra, Mkarrow2X
	IniWrite, %mkarrow2_y%, abra.ini, Abra, Mkarrow2Y
	IniWrite, %vending1_x%, abra.ini, Abra, Vending1X
	IniWrite, %vending1_y%, abra.ini, Abra, Vending1Y
	IniWrite, %vending1_c%, abra.ini, Abra, Vending1C
	IniWrite, %vending2_x%, abra.ini, Abra, Vending2X
	IniWrite, %vending2_y%, abra.ini, Abra, Vending2Y
return

ButtonTESTSOUND1:
	SoundPlay Sound.wav
return

ButtonTESTSOUND2:
	SoundPlay Sound2.wav
return

GuiClose:
GuiEscape:
	ExitApp



