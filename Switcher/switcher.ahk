#ifwinActive RagnaChan game client | Gepard Shield 2.0 (^-_-^)

#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

#include ButtonSettings.ahk

Gui,+AlwaysOnTop
Gui, Add, Button, x0 y0 w120 h20 ,ABRA
Gui, Add, Button, x0 y20 w120 h20 ,BARD
Gui, Add, Button, x0 y40 w120 h20 ,DANCER
Gui, Add, Button, x0 y60 w120 h20 ,DD
Gui, Add, Button, x0 y80 w120 h20 ,GOSPEL
Gui, Add, Button, x0 y100 w120 h20 ,OTHER
Gui, Add, Button, x0 y140 w120 h20 ,RELOAD

Gui, Add, Button, x120 y0 w60 h20, RES1
Gui, Add, Button, x120 y20 w60 h20, RES2
Gui, Add, Button, x120 y40 w60 h20, RES3
Gui, Add, Button, x120 y60 w60 h20, RES4
Gui, Add, Button, x120 y80 w60 h20, RES5
Gui, Add, Button, x120 y100 w60 h20, RES6

Gui, Show, ,Simple Switch

w1id:=-1
w2id:=-1
w3id:=-1
w4id:=-1
w5id:=-1
w6id:=-1

^!F1::
	WinGet w1id, ID
	MsgBox, Saved
return

^!F2::
	WinGet w2id, ID
	MsgBox, Saved
return

^!F3::
	WinGet w3id, ID
	MsgBox, Saved
return

^!F4::
	WinGet w4id, ID
	MsgBox, Saved
return

^!F5::
	WinGet w5id, ID
	MsgBox, Saved
return

^!F6::
	WinGet w6id, ID
	MsgBox, Saved
return

ButtonABRA:
	WinActivate, ahk_id %w1id%
	Sleep, 200
	WinGet, id, ID
return

ButtonBARD:
	WinActivate, ahk_id %w2id%
		Sleep, 200
	WinGet, id, ID
return

ButtonDANCER:
	WinActivate, ahk_id %w3id%
		Sleep, 200
	WinGet, id, ID
return

ButtonDD:
	WinActivate, ahk_id %w4id%
		Sleep, 200
	WinGet, id, ID
return

ButtonGOSPEL:
	WinActivate, ahk_id %w5id%
		Sleep, 200
	WinGet, id, ID
return

ButtonOTHER:
	WinActivate, ahk_id %w6id%
		Sleep, 200
	WinGet, id, ID
return



ButtonRES1:
	GOGL(w1id)
return

ButtonRES2:
	GOGL(w2id)
return

ButtonRES3:
	GOGL(w3id)
return

ButtonRES4:
	GOGL(w4id)
return

ButtonRES5:
	GOGL(w5id)
return

ButtonRES6:
	GOGL(w6id)
return

ButtonRELOAD:
	Reload
	return

GuiClose:
GuiEscape:
	ExitApp

