#ifwinActive RagnaChan game client | Gepard Shield 2.0 (^-_-^)

#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
;SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

Gui,+AlwaysOnTop
Gui, Add, Button, x0 y0 w120 h20 ,RESUME
Gui, Add, Button, x0 y20 w120 h20 ,PAUSE
Gui, Add, Button, x0 y60 w120 h20 ,RELOAD


Gui, Show, ,Simple Switch

Loop
{	
	IfWinNotActive, RagnaChan game client | Gepard Shield 2.0 (^-_-^)
	{
		Sleep, 10
		continue
	}
	

	MaxHP:=readMemory(0x83e1b8)
	MaxSP:=readMemory(0x83e1c0)

	CurHP:=readMemory(0x83e1b4)
	CurSP:=readMemory(0x83e1bc)	

	percentHP:= CurHP*100/MaxHP
	percentSP:= CurSP*100/MaxSP

	ToolTip, %percentHP% %percentSP%, 300, 300


	if(CurHP = 0)
	{
		Sleep, 100
		continue
	}

	if(percentHP < 90)
	{
		Send, {F1}
		Sleep, 100
		continue
	}

	if(percentSP < 30)
	{
		Send, {F2}
		Sleep, 100
		continue
	}

	Sleep, 100
}


ButtonRESUME:
	Pause, off
return

ButtonPAUSE:
	Pause, on
return


ButtonRELOAD:
	Reload
	return

GuiClose:
GuiEscape:
	ExitApp



ReadMemory(MADDRESS=0)
{
	Static OLDPROC, ProcessHandle
	VarSetCapacity(MVALUE,4,0)
	WinGet, pid, pid
	ProcessHandle := ( ProcessHandle ? 0*(closed:=DllCall("CloseHandle"
	,"UInt",ProcessHandle)) : 0 )+(pid ? DllCall("OpenProcess"
	,"Int",16,"Int",0,"UInt",pid) : 0)
	If (ProcessHandle) && DllCall("ReadProcessMemory","UInt"
	,ProcessHandle,"UInt",MADDRESS,"Str",MVALUE,"UInt",4,"UInt *",0)
	return *(&MVALUE+3)<<24 | *(&MVALUE+2)<<16 | *(&MVALUE+1)<<8 | *(&MVALUE)
	return !ProcessHandle ? "Handle Closed: " closed : "Fail"
}